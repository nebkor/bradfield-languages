use std::{
    collections::HashMap,
    fmt::{Debug, Display},
    ops::Deref,
};

use crate::{
    parser::{Expr, Stmt},
    token_error, Errors, Literal, Token, TokenType,
};

#[derive(Clone)]
pub enum Callable {
    Lox(Box<Stmt>),
    Native(fn(&[Value]) -> Result<Value, Errors>),
}

impl Debug for Callable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Callable::Lox(_) => {
                write!(f, "<Lox function>")
            }
            Callable::Native(_) => write!(f, "<Native function>"),
        }
    }
}

impl PartialEq for Callable {
    fn eq(&self, other: &Self) -> bool {
        matches!(
            (self, other),
            (Callable::Lox(_), Callable::Lox(_)) | (Callable::Native(_), Callable::Native(_))
        )
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Value {
    Nil,
    Boolean(bool),
    Number(f64),
    String(String),
    Function(Token, Vec<Token>, Callable), /* box the callable to keep the size of the
                                            * enum down */
}

impl Eq for Value {}

impl Default for Value {
    fn default() -> Self {
        Value::Nil
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Value::Nil => "nil".to_string(),
            Value::Boolean(b) => {
                if *b {
                    "true".to_string()
                } else {
                    "false".to_string()
                }
            }
            Value::Number(n) => {
                format!("{}", n)
            }
            Value::String(s) => s.clone(),
            Value::Function(name, args, _) => format!("<{}({})>", name.lexeme, format_args(args)),
        };
        write!(f, "{}", s)
    }
}

impl Value {
    pub fn arity(&self) -> Result<usize, ()> {
        if let Value::Function(_, args, _) = self {
            Ok(args.len())
        } else {
            Err(())
        }
    }
}

#[allow(dead_code)]
mod prelude {
    /// built-in functions and constants
    use super::Value;
    use crate::Errors;

    pub fn butts_fn(_arg: &[Value]) -> Result<Value, Errors> {
        Ok(Value::String("butts are stinky".to_string()))
    }

    pub fn clock_fn(_arg: &[Value]) -> Result<Value, Errors> {
        use std::time::{SystemTime, UNIX_EPOCH};
        let t = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis() as f64;
        let v = Value::Number(t);
        Ok(v)
    }
}

#[derive(Clone, Debug, Default)]
pub struct Environment {
    values: HashMap<String, Vec<Value>>,
    globals: HashMap<String, Value>,
}

impl Environment {
    pub fn new() -> Self {
        let mut globals = HashMap::new();

        let clock: Value = {
            let tok = Token::new(
                TokenType::IDENTIFIER,
                "clock",
                crate::Literal::String("clock".to_string()),
                0,
            );
            let callable = Callable::Native(prelude::clock_fn);
            Value::Function(tok, vec![], callable)
        };

        let butts: Value = {
            let tok = Token::new(
                TokenType::IDENTIFIER,
                "butts",
                crate::Literal::String("butts".to_string()),
                0,
            );
            let callable = Callable::Native(prelude::butts_fn);
            Value::Function(tok, vec![], callable)
        };

        globals.insert("butts".to_string(), butts);
        globals.insert("clock".to_string(), clock);

        Self {
            values: HashMap::default(),
            globals,
        }
    }

    pub fn define(&mut self, name: String, value: Value) {
        let v = self.values.entry(name).or_insert_with(Vec::new);
        v.push(value);
    }

    pub fn get_var(&self, name: &str) -> Option<&Value> {
        if let Some(local) = self.values.get(name).and_then(|v| v.last()) {
            Some(local)
        } else {
            self.globals.get(name)
        }
    }

    pub fn pop_var(&mut self, name: &str) {
        let v = self.values.get_mut(name).unwrap();
        v.pop();
        if v.is_empty() {
            self.values.remove(name);
        }
    }

    pub fn assign(&mut self, name: &str, value: Value) -> Result<(), ()> {
        if let Some(v) = self.values.get_mut(name) {
            if let Some(last) = v.last_mut() {
                *last = value;
                Ok(())
            } else {
                unreachable!()
            }
        } else {
            Err(())
        }
    }
}

pub fn interpret(statements: &[Stmt], env: &mut Environment) -> Option<Value> {
    for statement in statements {
        match statement {
            Stmt::Expression(e) | Stmt::Print(e) => match evaluate(e, env) {
                Ok(r) => {
                    if let Stmt::Print(_) = statement {
                        println!("{}", r);
                    }
                }
                Err(e) => {
                    if let Errors::RuntimeError(t, m) = e {
                        token_error(t, &m);
                    } else {
                        panic!("what the fuck?");
                    }
                }
            },
            // variable declaration
            Stmt::Var(name, expr) => {
                // first see if we have an expression to evaluate for a Value
                if let Some(value) = expr {
                    match evaluate(value, env) {
                        Ok(v) => {
                            env.define(name.lexeme.clone(), v);
                        }
                        Err(e) => {
                            if let Errors::RuntimeError(t, m) = e {
                                token_error(t, &m);
                            } else {
                                panic!("what the fuck?");
                            }
                        }
                    }
                } else {
                    // store a Nil.
                    env.define(name.lexeme.clone(), Value::Nil);
                }
            }
            Stmt::While(condition, body) => loop {
                match evaluate(condition, env) {
                    Ok(val) => {
                        if is_truthy(&val) {
                            if let Some(v) = interpret(&[*body.clone()], env) {
                                return Some(v);
                            }
                        } else {
                            break;
                        }
                    }
                    Err(e) => {
                        if let Errors::RuntimeError(t, msg) = e {
                            token_error(t, &msg);
                            break;
                        } else {
                            unreachable!()
                        }
                    }
                }
            },
            Stmt::Block(statements) => {
                let mut decls = Vec::new();
                for statement in statements.iter() {
                    if let Stmt::Var(name, _) = statement {
                        decls.push(name.lexeme.clone());
                    }
                }
                if let Some(v) = interpret(statements, env) {
                    for decl in decls {
                        env.pop_var(&decl);
                    }
                    return Some(v);
                }
                // OK, pop our bindings off:
                for decl in decls {
                    env.pop_var(&decl);
                }
            }
            Stmt::If(test, then, otherwise) => match evaluate(test, env) {
                Ok(val) => {
                    if is_truthy(&val) {
                        if let Some(v) = interpret(&[*then.clone()], env) {
                            return Some(v);
                        }
                    } else if let Some(otherwise) = otherwise {
                        if let Some(v) = interpret(&[*otherwise.clone()], env) {
                            return Some(v);
                        }
                    }
                }
                Err(e) => {
                    if let Errors::RuntimeError(t, m) = e {
                        token_error(t, &m);
                    } else {
                        unreachable!("seriously, runtime errors ONLY!");
                    }
                }
            },
            Stmt::Function(name, params, body) => {
                let callable = Callable::Lox(body.clone());
                let fun = Value::Function(name.clone(), params.clone(), callable);
                env.define(name.lexeme.clone(), fun);
            }
            Stmt::Return(_, expr) => {
                if let Some(e) = expr {
                    match evaluate(e, env) {
                        Ok(v) => return Some(v),
                        Err(Errors::RuntimeError(t, msg)) => {
                            token_error(t, &msg);
                        }
                        _ => unreachable!(),
                    }
                }
            }
        }
    }
    None
}

fn evaluate(expr: &Expr, env: &mut Environment) -> Result<Value, Errors> {
    use Value::{Boolean, Number};

    match expr {
        Expr::Literal(l) => match l {
            Literal::Bool(b) => Ok(Boolean(*b)),
            Literal::Number(n) => Ok(Number(*n)),
            Literal::String(s) => Ok(Value::String(s.clone())),
            Literal::Nil => Ok(Value::Nil),
        },
        Expr::Logical(left, op, right) => {
            let left = evaluate(left, env)?;
            if op.ttype == TokenType::OR {
                if is_truthy(&left) {
                    return Ok(left);
                }
            } else if !is_truthy(&left) {
                return Ok(left);
            }
            evaluate(right, env)
        }
        Expr::Grouping(e) => evaluate(e, env),
        Expr::Variable(v) => {
            if let Some(v) = env.get_var(&v.lexeme) {
                Ok(v.clone())
            } else {
                let msg = format!("Undefined variable '{}'.", v.lexeme);
                let e = Errors::RuntimeError(v.clone(), msg);
                Err(e)
            }
        }
        Expr::Assignment(name, expr) => {
            let value = evaluate(expr, env)?;
            if env.assign(&name.lexeme, value.clone()).is_ok() {
                Ok(value)
            } else {
                let e = Errors::RuntimeError(
                    name.clone(),
                    format!("Undefined variable '{}'.", name.lexeme),
                );
                Err(e)
            }
        }
        Expr::Unary(op, e) => {
            let right = evaluate(e, env)?;
            match op.ttype {
                TokenType::MINUS => {
                    if let Number(n) = right {
                        Ok(Number(-1.0 * n))
                    } else {
                        let e = Errors::RuntimeError(
                            op.clone(),
                            format!("Cannot minus-negate a non-number, {:?}", right),
                        );
                        Err(e)
                    }
                }
                TokenType::BANG => Ok(Boolean(!is_truthy(&right))),
                _ => panic!(),
            }
        }
        Expr::Binary(left, op, right) => {
            use TokenType::{
                BANG_EQUAL, EQUAL_EQUAL, GREATER, GREATER_EQUAL, LESS, LESS_EQUAL, MINUS, PLUS,
                SLASH, STAR,
            };
            let left = evaluate(left, env)?;
            let right = evaluate(right, env)?;
            match op.ttype {
                MINUS | SLASH | STAR => {
                    if let (Number(l), Number(r)) = (&left, &right) {
                        match op.ttype {
                            MINUS => Ok(Number(l - r)),
                            SLASH => Ok(Number(l / r)),
                            STAR => Ok(Number(l * r)),
                            _ => unreachable!(),
                        }
                    } else {
                        let e = Errors::RuntimeError(
                            op.clone(),
                            format!("Cannot apply '{}' to {:?} and {:?}", op.lexeme, left, right),
                        );
                        Err(e)
                    }
                }
                PLUS => {
                    if let (Number(l), Number(r)) = (&left, &right) {
                        return Ok(Number(l + r));
                    }
                    if let (Value::String(l), Value::String(r)) = (&left, &right) {
                        return Ok(Value::String(format!("{}{}", l, r)));
                    }
                    let e = Errors::RuntimeError(
                        op.clone(),
                        format!("Cannot add {:?} to {:?}", left, right),
                    );
                    Err(e)
                }
                GREATER | GREATER_EQUAL | LESS | LESS_EQUAL => {
                    if let (Number(l), Number(r)) = (&left, &right) {
                        match op.ttype {
                            GREATER => Ok(Boolean(l > r)),
                            GREATER_EQUAL => Ok(Boolean(l >= r)),
                            LESS => Ok(Boolean(l < r)),
                            LESS_EQUAL => Ok(Boolean(l <= r)),
                            _ => unreachable!(),
                        }
                    } else {
                        let e = Errors::RuntimeError(
                            op.clone(),
                            format!("Cannot compare {:?} with {:?}", left, right),
                        );
                        Err(e)
                    }
                }
                BANG_EQUAL => Ok(Boolean(left != right)),
                EQUAL_EQUAL => Ok(Boolean(left == right)),
                _ => todo!(),
            }
        }
        Expr::Call(callee, paren, arguments) => {
            let callee = evaluate(callee, env)?;
            if let Ok(arity) = callee.arity() {
                let arg_len = arguments.len();
                if arg_len != arity {
                    let e = Errors::RuntimeError(
                        paren.clone(),
                        format!(
                            "Expected {} arguments, but got {}, for {}",
                            arity, arg_len, callee
                        ),
                    );
                    return Err(e);
                }
            } else {
                let e = Errors::RuntimeError(
                    paren.clone(),
                    "Can only call functions and classes.".to_string(),
                );
                return Err(e);
            }

            let mut argv = Vec::new();
            for e in arguments {
                argv.push(evaluate(e, env)?);
            }

            // finish evaluating a call invocation
            call(&callee, &argv, env)
        }
    }
}

fn call(callable: &Value, args: &[Value], env: &mut Environment) -> Result<Value, Errors> {
    if let Value::Function(_nam, arg_decl, body) = callable {
        match body {
            Callable::Native(fun) => fun(args),
            Callable::Lox(body) => {
                for (decl, arg) in arg_decl.iter().map(|t| &t.lexeme).zip(args.iter()) {
                    env.define(decl.clone(), arg.clone());
                }
                let mut val = Value::Nil;
                if let Some(v) = interpret(&[body.deref().to_owned()], env) {
                    val = v;
                }

                // OK now pop the param decls out of the local env
                for decl in arg_decl.iter() {
                    env.pop_var(&decl.lexeme);
                }

                Ok(val)
            }
        }
    } else {
        unreachable!()
    }
}

fn is_truthy(val: &Value) -> bool {
    match val {
        Value::Nil => false,
        Value::Boolean(b) => *b,
        _ => true,
    }
}

fn format_args(args: &[Token]) -> String {
    let mut ret = String::new();
    let last = args.len().saturating_sub(1);
    for (i, s) in args.iter().enumerate() {
        if i < last {
            ret.push_str(&format!("{}, ", s));
        } else {
            ret.push_str(&format!("{}", s));
        }
    }
    ret
}
