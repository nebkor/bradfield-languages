use std::{
    collections::HashMap,
    fmt::{Debug, Display},
};

use crate::util::scanning_error;

#[derive(Clone)]
pub enum Literal {
    String(String),
    Number(f64),
    Bool(bool),
    Nil,
}

impl PartialEq for Literal {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::String(s), Self::String(o)) => s == o,
            (Self::Bool(s), Self::Bool(o)) => s == o,
            (Self::Number(s), Self::Number(o)) => s <= o && o <= s, // unsound!!
            (Self::Nil, Self::Nil) => true,
            _ => false,
        }
    }
}

impl Eq for Literal {}

const KEYTUP: [(&str, TokenType); 16] = [
    ("and", TokenType::AND),
    ("class", TokenType::CLASS),
    ("else", TokenType::ELSE),
    ("false", TokenType::FALSE),
    ("for", TokenType::FOR),
    ("fun", TokenType::FUN),
    ("if", TokenType::IF),
    ("nil", TokenType::NIL),
    ("or", TokenType::OR),
    ("print", TokenType::PRINT),
    ("return", TokenType::RETURN),
    ("super", TokenType::SUPER),
    ("this", TokenType::THIS),
    ("true", TokenType::TRUE),
    ("var", TokenType::VAR),
    ("while", TokenType::WHILE),
];

#[allow(non_camel_case_types, clippy::upper_case_acronyms)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TokenType {
    // Single-character tokens.
    LEFT_PAREN,
    RIGHT_PAREN,
    LEFT_BRACE,
    RIGHT_BRACE,
    COMMA,
    DOT,
    MINUS,
    PLUS,
    SEMICOLON,
    SLASH,
    STAR,

    // One or two character tokens.
    BANG,
    BANG_EQUAL,
    EQUAL,
    EQUAL_EQUAL,
    GREATER,
    GREATER_EQUAL,
    LESS,
    LESS_EQUAL,

    // Literals.
    IDENTIFIER,
    STRING,
    NUMBER,

    // Keywords.
    AND,
    CLASS,
    ELSE,
    FALSE,
    FUN,
    FOR,
    IF,
    NIL,
    OR,
    PRINT,
    RETURN,
    SUPER,
    THIS,
    TRUE,
    VAR,
    WHILE,

    EOF,
}

#[derive(Clone, PartialEq, Eq)]
pub struct Token {
    pub ttype: TokenType,
    pub lexeme: String,
    pub literal: Literal,
    pub location: usize,
}

impl Token {
    pub fn new(ttype: TokenType, lexeme: &str, literal: Literal, location: usize) -> Self {
        Token {
            ttype,
            literal,
            location,
            lexeme: lexeme.to_owned(),
        }
    }
}

impl Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} {} {}", &self.ttype, &self.lexeme, &self.literal)
    }
}

impl Debug for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", &self)
    }
}

impl Display for Literal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Literal::Number(n) => format!("Number({})", n),
            Literal::String(s) => format!("String({})", s),
            Literal::Nil => "Nil".to_string(),
            Literal::Bool(b) => format!("{}", b),
        };
        write!(f, "{}", s)
    }
}

impl Debug for Literal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", &self)
    }
}

pub struct Scanner {
    source: String,
    tokens: Vec<Token>,
    start: usize,
    current: usize,
    line: usize,
    keywords: HashMap<&'static str, TokenType>,
}

impl Scanner {
    pub fn new(source: &str) -> Self {
        let mut scanner = Scanner {
            source: source.to_owned(),
            tokens: vec![],
            start: 0,
            current: 0,
            line: 1,
            keywords: KEYTUP.iter().cloned().collect(),
        };
        scanner.tokenize();
        scanner
    }

    pub fn tokens(&self) -> &Vec<Token> {
        &self.tokens
    }

    fn done(&self) -> bool {
        self.current >= self.source.len()
    }

    fn tokenize(&mut self) {
        while !self.done() {
            self.start = self.current;
            self.scan_token();
        }
        self.push_token(TokenType::EOF);
    }

    fn scan_token(&mut self) {
        if let Some(c) = self.advance() {
            match c {
                '(' => self.push_token(TokenType::LEFT_PAREN),
                ')' => self.push_token(TokenType::RIGHT_PAREN),
                '{' => self.push_token(TokenType::LEFT_BRACE),
                '}' => self.push_token(TokenType::RIGHT_BRACE),
                ',' => self.push_token(TokenType::COMMA),
                '.' => self.push_token(TokenType::DOT),
                '-' => self.push_token(TokenType::MINUS),
                '+' => self.push_token(TokenType::PLUS),
                ';' => self.push_token(TokenType::SEMICOLON),
                '*' => self.push_token(TokenType::STAR),
                // double character tokens
                '!' => {
                    if self.matches('=') {
                        self.push_token(TokenType::BANG_EQUAL);
                    } else {
                        self.push_token(TokenType::BANG);
                    }
                }
                '=' => {
                    if self.matches('=') {
                        self.push_token(TokenType::EQUAL_EQUAL);
                    } else {
                        self.push_token(TokenType::EQUAL);
                    }
                }
                '<' => {
                    if self.matches('=') {
                        self.push_token(TokenType::LESS_EQUAL);
                    } else {
                        self.push_token(TokenType::LESS);
                    }
                }
                '>' => {
                    if self.matches('=') {
                        self.push_token(TokenType::GREATER_EQUAL);
                    } else {
                        self.push_token(TokenType::GREATER);
                    }
                }
                // '/' is tricky because division and comments
                '/' => {
                    if self.matches('/') {
                        // chomp them comments
                        while self.peek(0) != '\n' && !self.done() {
                            self.advance();
                        }
                    } else {
                        self.push_token(TokenType::SLASH);
                    }
                }

                // whitespace is just ignored
                ' ' => {}
                '\r' => {}
                '\t' => {}
                // newline bumps the line, though
                '\n' => {
                    self.line += 1;
                }

                // strings are tricky
                '"' => self.scan_string(),

                c if is_digit(c) => self.scan_number(),

                c if is_alpha(c) => self.scan_identifier(),

                _ => {
                    crate::scanning_error(self.line, "Unexpected character.");
                }
            }
        }
    }

    fn advance(&mut self) -> Option<char> {
        let c = self.source.chars().nth(self.current);
        self.current += 1;
        c
    }

    fn peek(&self, ahead: usize) -> char {
        if let Some(c) = self.source.chars().nth(self.current + ahead) {
            c
        } else {
            '\0'
        }
    }

    fn matches(&mut self, expected: char) -> bool {
        if self.done() {
            return false;
        }
        // we're not done, so there's guaranteed to be something there at current
        let actual = self.source.chars().nth(self.current).unwrap();
        if actual != expected {
            return false;
        }
        self.current += 1;
        true
    }

    fn scan_string(&mut self) {
        while self.peek(0) != '"' && !self.done() {
            if self.peek(0) == '\n' {
                self.line += 1;
            }
            self.advance();
        }

        if self.done() {
            scanning_error(self.line, "Unterminated string.");
            return;
        }
        // grab the closing '"'
        self.advance();

        let string = self.current_lexeme();
        let last = string.len() - 1;
        let last = last.max(1);
        let string = &string[1..last];

        let literal = Literal::String(string.to_string());
        self.push_literal(literal);
    }

    fn scan_number(&mut self) {
        while is_digit(self.peek(0)) {
            self.advance();
        }

        let more_num = self.peek(0) == '.' && is_digit(self.peek(1));
        if more_num {
            // grab the decimal point
            self.advance();
            // grab the stuff after the decimal
            while is_digit(self.peek(0)) {
                self.advance();
            }
        }

        let lexeme = self.current_lexeme();
        let num: f64 = lexeme.parse().unwrap();
        let literal = Literal::Number(num);
        self.push_literal(literal);
    }

    fn scan_identifier(&mut self) {
        while is_alphanumeric(self.peek(0)) {
            self.advance();
        }
        let lexeme = self.current_lexeme();
        let ttype = if let Some(&typ) = self.keywords.get(lexeme) {
            typ
        } else {
            TokenType::IDENTIFIER
        };

        // bools have a value to encode for parsing later
        match ttype {
            TokenType::TRUE => {
                let literal = Literal::Bool(true);
                self.push_literal(literal);
            }
            TokenType::FALSE => {
                let literal = Literal::Bool(false);
                self.push_literal(literal);
            }
            _ => self.push_token(ttype),
        }
    }

    fn current_lexeme(&self) -> &str {
        &self.source[self.start..self.current]
    }

    fn push_literal(&mut self, literal: Literal) {
        let ttype = match literal {
            Literal::Number(_) => TokenType::NUMBER,
            Literal::String(_) => TokenType::STRING,
            Literal::Bool(b) => {
                if b {
                    TokenType::TRUE
                } else {
                    TokenType::FALSE
                }
            }
            Literal::Nil => {
                scanning_error(
                    self.line,
                    "ICE: Tried to push_literal() Literal::Nil in scanner",
                );
                crate::exit(1);
            }
        };
        let lexeme = self.current_lexeme();
        let token = Token::new(ttype, lexeme, literal, self.line);
        self.tokens.push(token);
    }

    fn push_token(&mut self, ttype: TokenType) {
        let lexeme = self.current_lexeme().trim();
        let token = Token::new(ttype, lexeme, Literal::Nil, self.line);
        self.tokens.push(token);
    }
}

fn is_digit(c: char) -> bool {
    ('0'..='9').contains(&c)
}

fn is_alpha(c: char) -> bool {
    ('a'..='z').contains(&c) | ('A'..='Z').contains(&c) || c == '_'
}

fn is_alphanumeric(c: char) -> bool {
    is_alpha(c) || is_digit(c)
}
