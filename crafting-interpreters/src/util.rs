use std::sync::atomic::{AtomicBool, Ordering};

use crate::{
    parser::{Expr, Stmt},
    tokens::{Token, TokenType},
};

pub static HAD_ERROR: AtomicBool = AtomicBool::new(false);

pub type EResult = Result<Expr, Errors>; // expression return type; icing on the statement bagel
pub type SResult = Result<Stmt, Errors>;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Errors {
    ParseError,
    RuntimeError(Token, String),
}

pub fn scanning_error(line: usize, message: &str) {
    error_report(line, "", message);
}

fn error_report(line: usize, what: &str, message: &str) {
    eprintln!("[line {}] Error{}: {}", line, what, message);
    HAD_ERROR.store(true, Ordering::SeqCst);
}

pub fn token_error(token: Token, msg: &str) {
    let what = if token.ttype == TokenType::EOF {
        " at EOF".to_string()
    } else {
        format!(" at '{}'", token.lexeme)
    };
    error_report(token.location, &what, msg);
}
