use crate::{EResult, Errors, SResult, Token, TokenType};

pub type Boxpr = Box<Expr>;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Expr {
    Binary(Boxpr, Token, Boxpr),
    Grouping(Boxpr),
    Call(Boxpr, Token, Vec<Expr>),
    Literal(crate::Literal),
    Logical(Boxpr, Token, Boxpr),
    Unary(Token, Boxpr),
    Variable(Token),
    Assignment(Token, Boxpr),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Stmt {
    Expression(Expr),
    Print(Expr),
    Var(Token, Option<Expr>),
    While(Expr, Box<Stmt>),
    Block(Vec<Stmt>),
    If(Expr, Box<Stmt>, Option<Box<Stmt>>),
    Function(Token, Vec<Token>, Box<Stmt>),
    Return(Token, Option<Expr>),
}

pub struct Parser {
    tokens: Vec<Token>,
    current: usize,
}

impl Parser {
    pub fn new(tokens: Vec<Token>) -> Self {
        Parser { tokens, current: 0 }
    }

    pub fn parse(&mut self) -> Result<Vec<Stmt>, Errors> {
        let mut statments = Vec::new();
        while !self.done() {
            let declaration = self.declaration()?;
            statments.push(declaration);
        }
        Ok(statments)
    }

    fn declaration(&mut self) -> SResult {
        use TokenType::{FUN, VAR};
        match if self.matches(&[VAR]) {
            self.var_declaration()
        } else if self.matches(&[FUN]) {
            self.function_declaration("function")
        } else {
            self.statement()
        } {
            Ok(s) => Ok(s),
            Err(e) => {
                self.synchronize();
                Err(e)
            }
        }
    }

    fn function_declaration(&mut self, kind: &str) -> SResult {
        use TokenType::{COMMA, IDENTIFIER, LEFT_BRACE, LEFT_PAREN, RIGHT_PAREN};

        let name = self
            .consume(IDENTIFIER, &format!("Expect {} name.", kind))?
            .clone();
        self.consume(LEFT_PAREN, &format!("Expect '(' after {} name.", kind))?;

        let mut parameters = Vec::new();
        if !self.check(&RIGHT_PAREN) {
            loop {
                if parameters.len() > 254 {
                    error(self.peek().clone(), "Can't have more than 254 parameters.");
                }
                let param = self.consume(IDENTIFIER, "Expect parameter name.")?.clone();
                parameters.push(param);
                if !self.matches(&[COMMA]) {
                    break;
                }
            }
        }
        self.consume(RIGHT_PAREN, "Expect ')' after parameters.")?;
        self.consume(LEFT_BRACE, &format!("Expect '{{' before {} body.", kind))?;
        let body = self.block_statement()?;
        let fun = Stmt::Function(name, parameters, Box::new(body));
        Ok(fun)
    }

    fn statement(&mut self) -> SResult {
        use TokenType::{FOR, IF, LEFT_BRACE, PRINT, RETURN, WHILE};

        if self.matches(&[PRINT]) {
            self.print_statement()
        } else if self.matches(&[RETURN]) {
            self.return_statement()
        } else if self.matches(&[WHILE]) {
            self.while_statement()
        } else if self.matches(&[LEFT_BRACE]) {
            self.block_statement()
        } else if self.matches(&[FOR]) {
            self.for_statemment()
        } else if self.matches(&[IF]) {
            self.if_statement()
        } else {
            self.expr_statement()
        }
    }

    /// there's no AST node for a for statement, it's just sugar over a while
    /// loop
    fn for_statemment(&mut self) -> SResult {
        use TokenType::{LEFT_PAREN, RIGHT_PAREN, SEMICOLON, VAR};

        self.consume(LEFT_PAREN, "Expect '(' after 'for'.")?;

        let initializer = if self.matches(&[SEMICOLON]) {
            None
        } else if self.matches(&[VAR]) {
            Some(self.var_declaration()?)
        } else {
            Some(self.expr_statement()?)
        };

        let condition = if !self.check(&SEMICOLON) {
            Some(self.expression()?)
        } else {
            None
        };
        self.consume(SEMICOLON, "Expect ';' after loop condition.")?;

        // optional increment, final term
        let increment = if !self.check(&RIGHT_PAREN) {
            Some(self.expression()?)
        } else {
            None
        };
        self.consume(RIGHT_PAREN, "Expect ')' after for clauses.")?;

        let mut body = self.statement()?;

        if let Some(increment) = increment {
            body = Stmt::Block(vec![body, Stmt::Expression(increment)]);
        }

        let condition = if let Some(condition) = condition {
            condition
        } else {
            Expr::Literal(crate::Literal::Bool(true))
        };

        body = Stmt::While(condition, Box::new(body));

        if let Some(initializer) = initializer {
            body = Stmt::Block(vec![initializer, body]);
        };

        Ok(body)
    }

    fn while_statement(&mut self) -> SResult {
        use TokenType::{LEFT_PAREN, RIGHT_PAREN};

        self.consume(LEFT_PAREN, "Expect '(' after 'while'.")?;
        let condition = self.expression()?;
        self.consume(RIGHT_PAREN, "Expect ')' after condition.")?;
        let body = self.statement()?;

        Ok(Stmt::While(condition, Box::new(body)))
    }

    fn if_statement(&mut self) -> SResult {
        use TokenType::{ELSE, LEFT_PAREN, RIGHT_PAREN};
        self.consume(LEFT_PAREN, "Expect '(' after 'if'.")?;
        let test = self.expression()?;
        self.consume(RIGHT_PAREN, "Expect ')' after 'if condition.")?;
        let then = Box::new(self.statement()?);
        let otherwise = if self.matches(&[ELSE]) {
            Some(Box::new(self.statement()?))
        } else {
            None
        };

        Ok(Stmt::If(test, then, otherwise))
    }

    fn print_statement(&mut self) -> SResult {
        use TokenType::SEMICOLON;
        let value = self.expression()?;
        self.consume(SEMICOLON, "Expect ';' after value.")?;
        Ok(Stmt::Print(value))
    }

    fn return_statement(&mut self) -> SResult {
        use TokenType::SEMICOLON;

        let keyword = self.previous().clone();
        let expr = if self.check(&SEMICOLON) {
            None
        } else {
            Some(self.expression()?)
        };
        self.consume(SEMICOLON, "Expect ';' after return value.")?;
        Ok(Stmt::Return(keyword, expr))
    }

    fn block_statement(&mut self) -> SResult {
        use TokenType::RIGHT_BRACE;
        let mut statements = Vec::new();
        while !self.check(&RIGHT_BRACE) && !self.done() {
            statements.push(self.declaration()?);
        }
        self.consume(RIGHT_BRACE, "Expect '}' after block.")?;
        let statement = Stmt::Block(statements);
        Ok(statement)
    }

    fn expr_statement(&mut self) -> SResult {
        use TokenType::SEMICOLON;
        let expr = self.expression()?;
        self.consume(SEMICOLON, "Expect ';' after value.")?;
        Ok(Stmt::Expression(expr))
    }

    fn var_declaration(&mut self) -> SResult {
        use TokenType::{EQUAL, IDENTIFIER, SEMICOLON};
        let name = self.consume(IDENTIFIER, "Expect variable name.")?.clone();

        let initializer = if self.matches(&[EQUAL]) {
            Some(self.expression()?)
        } else {
            None
        };
        self.consume(SEMICOLON, "Expect ';' after variable declaration.")?;
        Ok(Stmt::Var(name, initializer))
    }

    fn expression(&mut self) -> EResult {
        self.assignment()
    }

    fn assignment(&mut self) -> EResult {
        use TokenType::EQUAL;

        let expr = self.logical_or()?;
        if self.matches(&[EQUAL]) {
            let equals = self.previous().clone();
            let value = self.assignment()?;
            if let Expr::Variable(v) = expr {
                return Ok(Expr::Assignment(v, Box::new(value)));
            } else {
                error(equals, "Invalid assignment target.");
            }
        }
        Ok(expr)
    }

    fn logical_or(&mut self) -> EResult {
        use TokenType::OR;
        let mut expr = self.logical_and()?;
        while self.matches(&[OR]) {
            let operator = self.previous().clone();
            let right = self.logical_and()?;
            expr = Expr::Logical(Box::new(expr.clone()), operator, Box::new(right));
        }
        Ok(expr)
    }

    fn logical_and(&mut self) -> EResult {
        use TokenType::AND;
        let mut expr = self.equality()?;
        while self.matches(&[AND]) {
            let operator = self.previous().clone();
            let right = Box::new(self.equality()?);
            expr = Expr::Logical(Box::new(expr), operator, right);
        }
        Ok(expr)
    }

    fn equality(&mut self) -> EResult {
        use TokenType::{BANG_EQUAL, EQUAL_EQUAL};

        let mut comparison = self.comparison()?;
        while self.matches(&[BANG_EQUAL, EQUAL_EQUAL]) {
            let operator = self.previous().clone();
            let right = self.comparison()?;
            comparison = Expr::Binary(Box::new(comparison.clone()), operator, Box::new(right));
        }
        Ok(comparison)
    }

    fn comparison(&mut self) -> EResult {
        use TokenType::{GREATER, GREATER_EQUAL, LESS, LESS_EQUAL};

        let mut term = self.term()?;

        while self.matches(&[GREATER, GREATER_EQUAL, LESS, LESS_EQUAL]) {
            let operator = self.previous().clone();
            let right = self.term()?;
            term = Expr::Binary(Box::new(term), operator, Box::new(right));
        }

        Ok(term)
    }

    fn term(&mut self) -> EResult {
        use TokenType::{MINUS, PLUS};

        let mut factor = self.factor()?;

        while self.matches(&[MINUS, PLUS]) {
            let operator = self.previous().clone();
            let right = self.factor()?;
            factor = Expr::Binary(Box::new(factor), operator, Box::new(right));
        }

        Ok(factor)
    }

    fn factor(&mut self) -> EResult {
        use TokenType::{SLASH, STAR};

        let mut unary = self.unary()?;
        while self.matches(&[SLASH, STAR]) {
            let operator = self.previous().clone();
            let right = self.unary()?;
            unary = Expr::Binary(Box::new(unary), operator, Box::new(right));
        }

        Ok(unary)
    }

    fn unary(&mut self) -> EResult {
        use TokenType::{BANG, MINUS};
        if self.matches(&[BANG, MINUS]) {
            let operator = self.previous().clone();
            let right = self.unary()?;
            return Ok(Expr::Unary(operator, Box::new(right)));
        }
        self.call()
    }

    fn call(&mut self) -> EResult {
        use TokenType::LEFT_PAREN;
        let mut expr = self.primary()?;
        loop {
            if self.matches(&[LEFT_PAREN]) {
                expr = self.finish_call(expr)?;
            } else {
                // this will make sense later, once there are properties on objects, according
                // to chapter 10.
                break;
            }
        }
        Ok(expr)
    }

    fn finish_call(&mut self, callee: Expr) -> EResult {
        use TokenType::{COMMA, RIGHT_PAREN};
        let mut arguments = Vec::new();
        if !self.check(&RIGHT_PAREN) {
            loop {
                arguments.push(self.expression()?.clone());
                // we're covering the case where `this` is part of the arguments
                if arguments.len() > 254 {
                    error(
                        self.peek().clone(),
                        "Cannot have more than 255 arguments for a function or method.",
                    );
                    break;
                }
                if !self.matches(&[COMMA]) {
                    break;
                }
            }
        }

        let paren = self
            .consume(RIGHT_PAREN, "Expect ')' after arguments.")?
            .clone();
        let callee = Box::new(callee);
        let call = Expr::Call(callee, paren, arguments);

        Ok(call)
    }

    fn primary(&mut self) -> EResult {
        use TokenType::{FALSE, IDENTIFIER, LEFT_PAREN, NIL, NUMBER, RIGHT_PAREN, STRING, TRUE};

        if self.matches(&[FALSE]) {
            return Ok(Expr::Literal(crate::Literal::Bool(false)));
        }
        if self.matches(&[TRUE]) {
            return Ok(Expr::Literal(crate::Literal::Bool(true)));
        }
        if self.matches(&[NIL]) {
            return Ok(Expr::Literal(crate::Literal::Nil));
        }

        if self.matches(&[NUMBER, STRING]) {
            let literal = self.previous().literal.clone();
            return Ok(Expr::Literal(literal));
        }

        if self.matches(&[IDENTIFIER]) {
            let ident = self.previous().clone();
            return Ok(Expr::Variable(ident));
        }

        if self.matches(&[LEFT_PAREN]) {
            let expr = self.expression()?;
            self.consume(RIGHT_PAREN, "Expect ')' after expression.")?;
            return Ok(Expr::Grouping(Box::new(expr)));
        }

        Err(error(self.peek().clone(), "Expect expression."))
    }

    // Parsing helpers
    fn consume(&mut self, ttype: TokenType, msg: &str) -> Result<&Token, Errors> {
        if self.check(&ttype) {
            return Ok(self.advance());
        }
        let token = self.peek().clone();
        Err(error(token, msg))
    }

    fn matches(&mut self, ttypes: &[TokenType]) -> bool {
        for t in ttypes {
            if self.check(t) {
                self.advance();
                return true;
            }
        }
        false
    }

    fn check(&self, ttype: &TokenType) -> bool {
        !self.done() && self.peek().ttype == *ttype
    }

    fn done(&self) -> bool {
        self.peek().ttype == TokenType::EOF
    }

    fn peek(&self) -> &Token {
        if let Some(t) = self.tokens.get(self.current) {
            t
        } else {
            crate::scanning_error(0, "ICE: tried to peek past end of token array in parser.");
            std::process::exit(65)
        }
    }

    fn previous(&self) -> &Token {
        &self.tokens[self.current - 1]
    }

    fn advance(&mut self) -> &Token {
        if !self.done() {
            self.current += 1;
        }
        self.previous()
    }

    fn synchronize(&mut self) {
        use TokenType::{CLASS, FOR, FUN, IF, PRINT, RETURN, SEMICOLON, VAR, WHILE};

        self.advance();
        while !self.done() {
            if self.previous().ttype == SEMICOLON {
                return;
            }
            let ttype = self.peek().ttype;
            match ttype {
                CLASS | FOR | FUN | IF | PRINT | RETURN | SEMICOLON | VAR | WHILE => return,
                _ => {
                    self.advance();
                }
            }
        }
    }
}

fn error(token: Token, msg: &str) -> Errors {
    crate::token_error(token, msg);
    Errors::ParseError
}
