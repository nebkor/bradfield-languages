use std::{
    env,
    fs::File,
    io::{stdin, stdout, Read, Write},
    process::exit,
    sync::atomic::Ordering,
};

mod tokens;
pub use tokens::*;

mod util;
use util::*;

mod parser;

mod interpreter;

fn main() {
    let oargs: Vec<String> = env::args().collect();
    let prog_name = &oargs[0];
    let args = &oargs[1..];

    let mut env = interpreter::Environment::new();

    match args.len() {
        0 => run_prompt(&mut env),
        1 => {
            let path = &args[0];
            let mut file = std::fs::File::open(std::path::Path::new(path)).unwrap();
            run_file(&mut file, &mut env);
        }
        _ => {
            println!("Usage: {} [script]", prog_name);
            exit(64);
        }
    }
}

fn run_file(script: &mut File, env: &mut interpreter::Environment) {
    let mut source = String::new();
    let _ = script.read_to_string(&mut source).unwrap();
    run(&source, env);

    if HAD_ERROR.load(Ordering::SeqCst) {
        exit(65);
    }
}

fn run_prompt(env: &mut interpreter::Environment) {
    loop {
        let mut line = String::new();
        print!("> ");
        let _ = stdout().flush();
        if let Ok(bytes) = stdin().read_line(&mut line) {
            if bytes == 0 {
                break;
            }
        }
        run(&line, env);
        HAD_ERROR.store(false, Ordering::SeqCst);
    }
}

fn run(source: &str, env: &mut interpreter::Environment) {
    let scanner = Scanner::new(source);
    let tokens = scanner.tokens();

    let mut parser = parser::Parser::new(tokens.clone());

    match parser.parse() {
        Ok(expr) => {
            interpreter::interpret(&expr, env);
        }
        Err(e) => {
            println!("{:?}", e);
        }
    }
}
