use std::io::{stdin, stdout, Write};

type Inputter = dyn Fn() -> i32;

mod scanner;

mod parser {
    use crate::{scanner::Number, TType, Token};

    enum Node {
        Number(Number),
        Unary(Token, Box<Node>),
        Grouping(Box<Node>),
        Binary(Box<Node>, Token, Box<Node>),
    }

    pub type Return = Result<Node, String>;

    pub struct Parser {
        tokens: Vec<Token>,
        current: usize,
    }

    impl Parser {
        pub fn new(tokens: Vec<Token>) -> Self {
            Parser { tokens, current: 0 }
        }

        pub fn parse(&mut self) -> Return {
            let e = self.expression()?;
            if !self.done() {
                return Err("couldn't finish parsing".to_string());
            }
            Ok(e)
        }

        fn expression(&mut self) -> Return {
            self.term()
        }

        fn term(&mut self) -> Return {
            use TType::{Minus, Plus};

            let mut factor = self.factor()?;

            while self.matches(&[Minus, Plus]) {
                let operator = self.previous().clone();
                let right = self.factor()?;
                factor = Node::Binary(Box::new(factor), operator, Box::new(right));
            }

            Ok(factor)
        }

        fn factor(&mut self) -> Return {
            use TType::{SLASH, STAR};

            let mut unary = self.unary()?;
            while self.matches(&[SLASH, STAR]) {
                let operator = self.previous().clone();
                let right = self.unary()?;
                unary = Node::Binary(Box::new(unary), operator, Box::new(right));
            }

            Ok(unary)
        }

        fn unary(&mut self) -> Return {
            use TType::{BANG, MINUS};
            if self.matches(&[BANG, MINUS]) {
                let operator = self.previous().clone();
                let right = self.unary()?;
                return Ok(Node::Unary(operator, Box::new(right)));
            }
            self.primary()
        }

        fn primary(&mut self) -> Return {
            use TType::{LeftParen, RightParen};

            if self.matches(&[Number]) {
                let literal = self.previous().literal.clone();
                return Ok(Node::Literal(literal));
            }

            if self.matches(&[LEFT_PAREN]) {
                let expr = self.expression()?;
                self.consume(RIGHT_PAREN, "Expect ')' after expression.")?;
                return Ok(Node::Grouping(Box::new(expr)));
            }

            Err(error(self.peek().clone(), "Expect expression."))
        }

        // Parsing helpers
        fn consume(&mut self, ttype: TType, msg: &str) -> Result<&Token, Errors> {
            if self.check(&ttype) {
                return Ok(self.advance());
            }
            let token = self.peek().clone();
            Err(error(token, msg))
        }

        fn matches(&mut self, ttypes: &[TType]) -> bool {
            for t in ttypes {
                if self.check(t) {
                    self.advance();
                    return true;
                }
            }
            false
        }

        fn check(&self, ttype: &TType) -> bool {
            !self.done() && self.peek().ttype == *ttype
        }

        fn done(&self) -> bool {
            self.peek().ttype == TType::EOF
        }

        fn peek(&self) -> &Token {
            if let Some(t) = self.tokens.get(self.current) {
                t
            } else {
                crate::scanning_error(0, "ICE: tried to peek past end of token array in parser.");
                std::process::exit(65)
            }
        }

        fn previous(&self) -> &Token {
            &self.tokens[self.current - 1]
        }

        fn advance(&mut self) -> &Token {
            if !self.done() {
                self.current += 1;
            }
            self.previous()
        }
    }
}

mod evaluator {
    // todo
}

use evaluator::*;
use parser::*;
use scanner::*;

trait Eval {
    fn eval(&self) -> Result<i32, String>;
}

enum BinOpType {
    Add,
    Mul,
}
enum SyntaxNode {
    BinOp(BinOpType, Box<SyntaxNode>, Box<SyntaxNode>),
    Value(i32),
    Print(Box<SyntaxNode>),
    Input(Box<Inputter>),
}

impl Eval for SyntaxNode {
    fn eval(&self) -> Result<i32, String> {
        match self {
            Self::BinOp(op, v1, v2) => match op {
                BinOpType::Add => Ok(v1.eval()? + v2.eval()?),
                BinOpType::Mul => Ok(v1.eval()? * v2.eval()?),
            },
            Self::Value(v) => Ok(*v),
            Self::Print(e) => {
                let res = e.eval()?;
                println!("{}", &res);
                Ok(res)
            }
            Self::Input(input) => Ok(input()),
        }
    }
}

fn input() -> i32 {
    let mut s = String::new();
    print!("Please enter a number: ");
    let _ = stdout().flush();
    stdin().read_line(&mut s).expect("oh snaps");
    if let Some('\n') = s.chars().next_back() {
        s.pop();
    }
    if let Some('\r') = s.chars().next_back() {
        s.pop();
    }
    s.parse().unwrap()
}

fn main() {
    /*
    Print(
     Plus(
      Times(
        Literal(13)
        Literal(2))
      Times(
        Input
        Literal(7)
      )
     )
    )
     */

    /*
    let v13 = SyntaxNode::Value(13);
    let v2 = SyntaxNode::Value(2);
    let t1 = SyntaxNode::BinOp(BinOpType::Mul, Box::new(v13), Box::new(v2));

    let v7 = SyntaxNode::Value(7);
    let t_input = SyntaxNode::Input(Box::new(input));
    let t2 = SyntaxNode::BinOp(BinOpType::Mul, Box::new(v7), Box::new(t_input));

    let plus = SyntaxNode::BinOp(BinOpType::Add, Box::new(t1), Box::new(t2));
    let print = SyntaxNode::Print(Box::new(plus));

    let _ = print.eval();
     */

    run_prompt();
}

fn run_prompt() {
    loop {
        let mut line = String::new();
        print!("> ");
        let _ = stdout().flush();
        if let Ok(bytes) = stdin().read_line(&mut line) {
            if bytes == 0 {
                break;
            }
        }
        run(&line);
    }
}

fn run(source: &str) {
    let scanner = Scanner::new(source.to_string());
    let tokens = scanner.tokens();

    for token in tokens {
        println!("{:?}", &token);
    }

    //let mut parser = parser::Parser::new(tokens.clone());

    // match parser.parse() {
    //     Ok(expr) => {
    //         interpreter::interpret(&expr);
    //     }
    //     Err(e) => {
    //         println!("{:?}", e);
    //     }
    // }
}
