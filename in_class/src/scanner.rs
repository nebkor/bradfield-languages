use std::fmt::Display;

// todo

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub enum Number {
    Int(i64),
    Float(f64),
}

impl Eq for Number {}

#[allow(clippy::upper_case_acronyms)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TType {
    Star,
    Slash,
    Plus,
    Minus,
    Carrot,
    LeftParen,
    RightParen,
    Dot,
    Value(Number),
    EOF,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Token {
    pos: usize,
    ttype: TType,
    lexeme: String,
}

impl Token {
    pub fn new(pos: usize, ttype: TType, lexeme: String) -> Self {
        Self { pos, ttype, lexeme }
    }

    /// Get a reference to the token's pos.
    pub fn pos(&self) -> usize {
        self.pos
    }

    /// Get a reference to the token's ttype.
    pub fn ttype(&self) -> &TType {
        &self.ttype
    }

    /// Get a reference to the token's lexeme.
    pub fn lexeme(&self) -> &str {
        self.lexeme.as_str()
    }
}

impl Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.lexeme())
    }
}

pub struct Scanner {
    source: String,
    tokens: Vec<Token>,
    start: usize,
    current: usize,
    line: usize,
}

impl Scanner {
    pub fn new(source: String) -> Self {
        let mut s = Self {
            source,
            tokens: vec![],
            start: 0,
            current: 0,
            line: 1,
        };
        s.tokenize();
        s
    }

    pub fn tokenize(&mut self) {
        while !self.done() {
            self.start = self.current;
            self.scan_token();
        }
        self.push_token(TType::EOF);
    }

    pub fn done(&self) -> bool {
        self.current >= self.source.len()
    }

    /// Get a reference to the scanner's tokens.
    pub fn tokens(&self) -> &[Token] {
        self.tokens.as_slice()
    }

    fn advance(&mut self) -> Option<char> {
        let c = self.source.chars().nth(self.current);
        self.current += 1;
        c
    }

    fn peek(&self, ahead: usize) -> char {
        if let Some(c) = self.source.chars().nth(self.current + ahead) {
            c
        } else {
            '\0'
        }
    }

    fn matches(&mut self, expected: char) -> bool {
        if self.done() {
            return false;
        }
        // we're not done, so there's guaranteed to be something there at current
        let actual = self.source.chars().nth(self.current).unwrap();
        if actual != expected {
            return false;
        }
        self.current += 1;
        true
    }

    fn scan_token(&mut self) {
        use TType::*;
        if let Some(c) = self.advance() {
            match c {
                '(' => self.push_token(LeftParen),
                ')' => self.push_token(RightParen),
                '.' => self.push_token(Dot),
                '-' => self.push_token(Minus),
                '+' => self.push_token(Plus),
                '*' => self.push_token(Star),
                '/' => self.push_token(Slash),
                '^' => self.push_token(Carrot),
                // whitespace is just ignored
                ' ' => {}
                '\r' => {}
                '\t' => {}
                // newline bumps the line, though
                '\n' => {
                    self.line += 1;
                }
                n if is_digit(n) => self.scan_number(),
                _ => {
                    eprintln!(
                        "you should not be here: '{}' at {:?}",
                        self.current_lexeme(),
                        (self.line, self.start)
                    );
                }
            }
        } else {
            eprintln!("what the fuck");
        }
    }

    fn push_token(&mut self, ttype: TType) {
        let lexeme = self.current_lexeme().trim();
        let pos = self.line;
        let token = Token::new(pos, ttype, lexeme.to_owned());
        self.tokens.push(token);
    }

    fn current_lexeme(&self) -> &str {
        &self.source[self.start..self.current]
    }

    fn scan_number(&mut self) {
        while is_digit(self.peek(0)) {
            self.advance();
        }

        let is_float = self.peek(0) == '.' && is_digit(self.peek(1));
        if is_float {
            // grab the decimal point
            self.advance();
            // grab the stuff after the decimal
            while is_digit(self.peek(0)) {
                self.advance();
            }
        }

        let lexeme = self.current_lexeme().to_owned();
        let sector = self.line;
        let ttype = if is_float {
            TType::Value(Number::Float(lexeme.parse().unwrap()))
        } else {
            TType::Value(Number::Int(lexeme.parse().unwrap()))
        };
        let number = Token::new(sector, ttype, lexeme);
        self.tokens.push(number);
    }
}

fn is_digit(c: char) -> bool {
    ('0'..='9').contains(&c)
}

fn is_alpha(c: char) -> bool {
    ('a'..='z').contains(&c) | ('A'..='Z').contains(&c) || c == '_'
}

fn is_alphanumeric(c: char) -> bool {
    is_alpha(c) || is_digit(c)
}
